//
//  BongoTestTests.swift
//  BongoTestTests
//
//  Created by Istiak Morsalin on 31/10/21.
//

import XCTest
@testable import BongoTest

class BongoTestTests: XCTestCase {
    private var termsOfServiceViewModel: TermsOfServiceViewModel!
    
    override func setUp() {
        super.setUp()
        termsOfServiceViewModel = TermsOfServiceViewModel()
    }
    
    func test_printLastCharacter_Of_String() {
        let mockData = "Istiak"
        let lastCharater = termsOfServiceViewModel.getLastCharacterOfString(htmlString: mockData)
        XCTAssertEqual(lastCharater, "k", "Can print last character successfully")
    }
    
    func test_getTenthCharacters_Of_String() {
        let mockData = "IstiakMorsalinIstiak"
        let getTenthCharaters = termsOfServiceViewModel.getTenthCharacters(htmlString: mockData)
        XCTAssertEqual(getTenthCharaters, ["a"], "Can find tenth character successfully")
    }
    
    func test_words_Of_String() {
        let mockData = "Its my life"
        let words = termsOfServiceViewModel.getWords(htmlString: mockData)
        XCTAssertEqual(words, ["Its", "my", "life"], "Can find tenth character successfully")
    }

    override func tearDown() {
        super.tearDown()
        termsOfServiceViewModel = nil
    }
}
