//
//  ViewController.swift
//  BongoTest
//
//  Created by Istiak Morsalin on 31/10/21.
//

import UIKit
import RxSwift
import RxCocoa
import NVActivityIndicatorView

class ViewController: UIViewController {
    @IBOutlet weak var tosTextView: UITextView!
    @IBOutlet weak var getDataButton: UIButton!
    @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
    
    var termsOfServiceViewModel = TermsOfServiceViewModel()
    let disposeBag = DisposeBag()
    
    // MARK: VC Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBindings()
    }
    
    @IBAction func getDataAction(_ sender: Any) {
        activityIndicator.startAnimating()
        termsOfServiceViewModel.fetchData()
    }
    
    private func setupBindings() {
        termsOfServiceViewModel.htmlData.subscribe({(event) in
            self.activityIndicator.stopAnimating()
            switch event {
            case .next(let element):
                self.tosTextView.text = element
            case .error(let error):
                //hide progress loader
                print(error)
            case .completed:
                //hide progress loader
                print("completed")
            }
        }).disposed(by: disposeBag)
    }
    
}

