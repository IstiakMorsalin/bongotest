//
//  BongoService.swift
//  BongoTest
//
//  Created by Istiak Morsalin on 31/10/21.
//

import Foundation
import Moya
import Alamofire

enum BongoService {
    case showTermsOfService
}

// MARK: - TargetType Protocol Implementation
extension BongoService: TargetType {
    var baseURL: URL { URL(string: "https://www.bioscopelive.com")! }
    var path: String {
        switch self {
        case .showTermsOfService:
            return "/en/tos"
        }
    }
    var method: Moya.Method {
        switch self {
        case .showTermsOfService:
            return .get
        }
    }
    var task: Task {
        switch self {
        case .showTermsOfService: // Send no parameters
            return .requestPlain
        }
    }
    var sampleData: Data {
        switch self {
        case .showTermsOfService:
            return "Half measures are as bad as nothing at all.".utf8Encoded
        }
    }
    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
}
// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }

    var utf8Encoded: Data { Data(self.utf8) }
}
