//
//  TermsOfServiceViewModel.swift
//  BongoTest
//
//  Created by Istiak Morsalin on 31/10/21.
//

import Foundation
import Moya
import RxSwift
import RxCocoa

class TermsOfServiceViewModel {
    var provider: MoyaProvider<BongoService>? = nil
    
    private var stringData: String!
    
    public let htmlData : PublishSubject<String> = PublishSubject()
    public let loading: PublishSubject<Bool> = PublishSubject()
    public let error : PublishSubject<Error> = PublishSubject()
    
    private let disposable = DisposeBag()
    
    init() {
        self.provider = MoyaProvider<BongoService>()
    }
    
    func fetchData() {
        provider?.request(.showTermsOfService) { result in
            self.loading.onNext(false)
            switch result {
            case let .success(response):
                do {
                    let statusCode = response.statusCode
                    print(statusCode)
                    let dataValue = try response.mapString()
                    self.stringData = dataValue
                    self.htmlData.onNext(dataValue)
                    let lastCharacter = self.getLastCharacterOfString(htmlString: self.stringData)
                    print("lastCharacter of the string is \(lastCharacter)")
                    let tenthCharArray = self.getTenthCharacters(htmlString: self.stringData)
                    print("Each TenthCharArray of the string is \(tenthCharArray)")
                    let words = self.getWords(htmlString: self.stringData)
                    print("Words \(words)")
                } catch let error {
                    print(error.localizedDescription)
                }
            case let .failure(error):
                self.error.onNext(error)
            }
        }
        
    }
    
    public func getLastCharacterOfString(htmlString: String) -> Substring {
        return htmlString.suffix(1)
    }
    
    func getTenthCharacters(htmlString: String) -> [Character] {
        let characters = Array(htmlString)
        var charArray: [Character] = []
        for (index, char) in characters.enumerated() {
            if(index % 10 == 0 && index > 0) {
                charArray.append(char)
            }
        }
        return charArray
    }
    
    func getWords(htmlString: String) -> [String] {
        let arr = htmlString.components(separatedBy: " ")
        var words: [String] = []
        arr.forEach { item in
            words.append(item)
        }
        return words
    }
}
